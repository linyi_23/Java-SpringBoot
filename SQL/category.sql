/*
Navicat MySQL Data Transfer

Source Server         : wyq
Source Server Version : 50627
Source Host           : localhost:3306
Source Database       : secondhand

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2020-12-14 22:59:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `c_Id` varchar(100) NOT NULL,
  `c_Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`c_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('c01', '笔记本电脑');
INSERT INTO `category` VALUES ('c02', '台式电脑');
INSERT INTO `category` VALUES ('c03', '平板电脑');
INSERT INTO `category` VALUES ('c04', '笔记本');
INSERT INTO `category` VALUES ('c05', '男士手表');
INSERT INTO `category` VALUES ('c06', '雨伞');
INSERT INTO `category` VALUES ('c07', '太阳伞');
INSERT INTO `category` VALUES ('c08', '太阳眼镜');
INSERT INTO `category` VALUES ('c09', '足球');
INSERT INTO `category` VALUES ('c10', '篮球');
INSERT INTO `category` VALUES ('c11', '乒乓球');
INSERT INTO `category` VALUES ('c12', '网球');
INSERT INTO `category` VALUES ('c13', '网球拍');
INSERT INTO `category` VALUES ('c14', '乒乓球拍');
INSERT INTO `category` VALUES ('c15', '足球鞋');
INSERT INTO `category` VALUES ('c16', '篮球鞋');
INSERT INTO `category` VALUES ('c17', '休闲鞋');
INSERT INTO `category` VALUES ('c18', '板鞋');
INSERT INTO `category` VALUES ('c19', '女士手提包');
INSERT INTO `category` VALUES ('c20', '冰箱');
INSERT INTO `category` VALUES ('c21', '沙发');
INSERT INTO `category` VALUES ('c22', '手机');
INSERT INTO `category` VALUES ('c23', '相机');
INSERT INTO `category` VALUES ('c24', '键盘');
INSERT INTO `category` VALUES ('c25', '鼠标');
INSERT INTO `category` VALUES ('c26', '屏幕');
INSERT INTO `category` VALUES ('c27', '旅行箱');
INSERT INTO `category` VALUES ('c28', '屏幕');
INSERT INTO `category` VALUES ('c29', '图书');
INSERT INTO `category` VALUES ('c30', '音乐专辑');
INSERT INTO `category` VALUES ('c31', '女士手表');
INSERT INTO `category` VALUES ('c33', '女装');
INSERT INTO `category` VALUES ('c34', '男装');
INSERT INTO `category` VALUES ('c35', '书包');
INSERT INTO `category` VALUES ('c36', '手办');
INSERT INTO `category` VALUES ('c37', '游戏手柄');
INSERT INTO `category` VALUES ('c38', '游戏机');
INSERT INTO `category` VALUES ('c39', '杯子');
INSERT INTO `category` VALUES ('c40', '床上桌');
